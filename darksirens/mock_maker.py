'''
creating mock merger data 22/01/2020
updating for dark sirens project 09/04/2021. time flies when you're having fun!
'''

# import general stuff
import os, sys
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
import random as r
from scipy.integrate import quad
from scipy.stats import truncnorm
from scipy.stats import norm
from scipy.optimize import minimize, root_scalar
from sklearn.neighbors import KernelDensity
from sklearn.model_selection import GridSearchCV
from copy import deepcopy
import pandas as pd #NHmod
import time
from tqdm import tqdm

from darksirens.survey.noisecalculation import noise
from darksirens.survey.telescope import survey
from darksirens.cosmology.cosmo import Cosmology
from darksirens.cosmology.lensing import Lensing
from darksirens.events.createevent import event_generator
from darksirens.events.numberofevents import event_number_calculator


#np.random.seed(42)

class compute_GWmock:

    def __init__(self,cosmo_params,progenitor,ABHpars,PBHpars,specs,outpath=None):
        print('')
        print('Initialising mock maker...')

        #GET COSMOLOGY
        self.cosmo = Cosmology(cosmo_params)
        d_C        = self.cosmo.functions['comoving_distance']
        d_L        = self.cosmo.functions['luminosity_distance']
        H          = self.cosmo.functions['Hubble']

        if progenitor == 'testing':
            self.PBHratio = 0
            print('Using testing case. N_tot = {}'.format(specs['Ngw']))
        else:
            Ncalc = event_number_calculator(self.cosmo,ABHpars,PBHpars,specs)

            print('')
            print('Computing number of events...')
            Nabh_exp  = Ncalc.NABHeventscalculator(ABHpars, self.cosmo, specs['z_max'], specs['Tobs']) #NHmod
            print('...N_ABH (expected)     = {}'.format(Nabh_exp))
            Nabh = np.random.poisson(Nabh_exp)
            print('...N_ABH (observed)     = {}'.format(Nabh))

            Npbh_exp  = Ncalc.NPBHeventscalculator(PBHpars, self.cosmo, specs['z_max'], specs['Tobs']) #NHmod
            print('...N_PBH (expected)     = {}'.format(Npbh_exp))
            Npbh = np.random.poisson(Npbh_exp)
            print('...N_PBH (observed)     = {}'.format(Npbh))

            Ntot     = Nabh+Npbh
            PBHratio = Npbh/Ntot
            print('...N_TOT     = {}'.format(Ntot))
            print('...PBH ratio = {}'.format(PBHratio))

            specs['Ngw'] = int(Ntot)
            self.PBHratio = PBHratio

        self.survey_specs = survey(specs)

        self.generic = event_generator(self.cosmo,ABHpars,PBHpars,progenitor,specs)


        #GENERATE N EVENTS
        print('CREATING EVENTS')
        self.events = self.generate_realization(progenitor,ABHpars,PBHpars,specs)


        #Initialize survey and noise
        print('COMPUTING NOISE')
        self.lensing = Lensing(self.cosmo)
        self.noise  = noise(specs, self.survey_specs, self.generic, d_L, lensing=self.lensing)


        #Getting mock data
        print('Producing mock catalogue of {} events.'.format(specs['Ngw']))
        start_time = time.time()
        self.uncut_mock = self.get_mock(self.events,d_L)
        end_time = (time.time()-start_time)/60
        print('Mock catalogue created in {:.2f} minutes.'.format(end_time))

        # remove events with SNR < user-defined value #NHmod
        # convert the dictionary to a pandas dataframe for easier operations
        mock_df = pd.DataFrame(self.uncut_mock)
        # create a new df which contains only those events with SNR > SNR_min
        mock_df = mock_df[mock_df.SNR > specs['SNR_min']]
        # reset the index of the df so there are no empty rows
        mock_df = mock_df.reset_index(drop=True)
        # convert the df back to a dictionary
        self.mock = mock_df.to_dict(orient='list')

        print('')
        print('Total events after SNR cut: {}'.format(len(self.mock['dL'])))

        #check on d_L/sigma


        if outpath != None:
            with open(os.path.join(os.getcwd(), outpath+'_GWdataset.txt'), 'w') as f:
                 f.write('#z dL sigma_dL SNR\n')

                 for ind in range(len(self.mock['z'])):
                     f.write("%f " %self.mock['z'][ind] + "%f " %self.mock['dL'][ind] +
                     "%f " %self.mock['dL_error'][ind] + "%f " %self.mock['SNR'][ind] + "\n")

            f.close()

            print('')
            print('Mock data saved in '+outpath+'_GWdataset.txt')



    def generate_realization(self,progenitor,ABHpars,PBHpars,specs):

        if progenitor == 'mixed':
            NPBH   = int(specs['Ngw']*self.PBHratio)
            events = []
            print('...doing PBHs')
            for ind in tqdm(range(NPBH)):
                events.append(self.generic.get_single_event('PBH-PBH'))
            print('...doing ABHs')
            for ind in tqdm(range(specs['Ngw']-NPBH)):
                events.append(self.generic.get_single_event('ABH-ABH'))

            print('Done events!')

        else:
            events = [self.generic.get_single_event(progenitor) for ind in range(specs['Ngw'])]

        return events


    def get_mock(self,events,d_L):

        points = self.noise.compute_noise(events,d_L)

        mock_features = deepcopy(points)

        return mock_features
