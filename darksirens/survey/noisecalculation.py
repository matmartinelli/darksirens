import os
dir_path = os.path.dirname(os.path.realpath(__file__))
import numpy as np
from scipy.interpolate import interp1d
from copy import deepcopy
from scipy.integrate import trapz
from tqdm import tqdm
from scipy.stats import truncnorm

clight  = 299792.458
kmtoMpc = 3.24078e-20


class noise:
    """
    This class handles the computation of the signal-to-noise ratio and errors for each event.
    """

    def __init__(self, specs, survey, generic_event, d_L, lensing=None):

        self.survey  = survey
        self.specs   = specs
        if 'data_spread' not in self.specs:
            self.specs['data_spread'] = False

        if specs['include_lensing'] is True:
            if lensing == None:
                raise ValueError('If you want to include lensing in the \
                calculation, then the lensing class must be included as an\
                argument of the noise class.')
            else:
                self.lensing = lensing

        ET_D         = np.loadtxt(dir_path + '/et_d.dat')
        f_etd_temp   = ET_D[:,0]
        asd_etd_temp = ET_D[:,1] # NH: does it make it faster to load it here?
        f_etd        = f_etd_temp[f_etd_temp>2]
        asd_etd      = asd_etd_temp[f_etd_temp>2]
        self.psd_etd = interp1d(f_etd, asd_etd*asd_etd,kind='linear',
                                bounds_error=False,fill_value=(np.inf,np.inf)) # note that x*x can be faster than x**2.

        self.minfreq = min(f_etd)

        #Create redshift for noise interpolation
        noisesamp = 100
        znoise = np.logspace(np.log10(self.specs['z_min']),np.log10(self.specs['z_max']),noisesamp)

        if hasattr(generic_event,'ABH_waveform'):
            ABH_snr_vec = [self.snrfunc(zi, generic_event.z0, generic_event.ABH_frequency, generic_event.ABH_waveform, d_L, self.psd_etd) for zi in znoise]
            self.snrsq_vec_ABH = interp1d(znoise,ABH_snr_vec,kind='cubic',bounds_error=False,fill_value='extrapolate')

        if hasattr(generic_event,'PBH_waveform'):
            PBH_snr_vec = [self.snrfunc(zi, generic_event.z0, generic_event.PBH_frequency, generic_event.PBH_waveform, d_L, self.psd_etd) for zi in znoise]
            self.snrsq_vec_PBH = interp1d(znoise,PBH_snr_vec,kind='cubic',bounds_error=False,fill_value='extrapolate')

        if hasattr(generic_event,'test_waveform'):
            testing_snr_vec = [self.snrfunc(zi, generic_event.z0, generic_event.test_frequency, generic_event.test_waveform, d_L, self.psd_etd) for zi in znoise]
            self.snrsq_vec_testing = interp1d(znoise,testing_snr_vec,kind='cubic',bounds_error=False,fill_value='extrapolate')


    def snrfunc(self, zz, zz0, ff0, waveform0, lum_dist, det_noises):
        """
        This function returns the signal-to-noise ratio for an event.

        :param zz: the redshift of the event.
        :param zz0: a reference redshift.
        :param ff0: the frequency of the gravitational wave emitted by the merger.
        :param waveform0: the waveform (i.e. the GW strain) for the event.
        :param lum_dist: the luminosity distance to the event.
        :param source_extinctions: the source extinctions
        :param det_noises: the total power spectral density for the telescope.
        """
        #det_noises_sq_arr = np.array([det_noises[kk](ff) for kk in range(len(det_noises))])
        ff     = ff0/(1+zz)
        ref_dL = lum_dist(zz0)
        new_dL = lum_dist(zz)
        scaled_waveform = interp1d(ff,waveform0*ref_dL/new_dL*(1+zz)**2,kind='cubic',bounds_error=False,fill_value=(0,0))

        integ = lambda x: 4*(np.square(np.real(scaled_waveform(x)))+np.square(np.imag(scaled_waveform(x))))/det_noises(x)

        #snrsq_vec = []
        #for arm,noise in enumerate(det_noises):
        #    snrsq_vec.append(4 * np.trapz(self.snr_integrand(zz, zz0, ff0, waveform0, lum_dist, noise), ff))

        integrand = integ(ff)
        snr =  np.trapz(integrand, ff)
        #snr = quad(integ, min(ff), max(ff))[0]

        return snr#np.array(snrsq_vec)


    def compute_noise(self,events,d_L):
        """
        This function uses the survey specifications to compute the total noise and signal-to-noise ratio for each event.
        It returns the final mock catalogue.

        :param events: an instance of the Events class
        :param d_L: the luminosity distance for each event.
        """

        #PF: Add randomness in this? I mean, shoudn't the observed distance
        # be the true distance plus something on the order of the error?

        #MMmod: to fix
        mock = {'z': [],
                'dL': [],
                'intrinsic_error': [],
                'lensing_error': [],
                'dL_error': [],
                'dL_unlensed': [],
                'SNR': [],
                'progenitor': []}


        for ind,event in enumerate(tqdm(events)):
            z         = event['redshift']
            z0        = event['z_reference']
            f0        = event['frequency']
            waveform0 = event['waveform']

            d_unlensed = d_L(z) # background (unlensed) luminosity distance

            if event['progenitor'] == 'ABH-ABH':
                snrsq_vec = [self.snrsq_vec_ABH(z)]*len(self.survey.source_extinctions[ind,:])
            elif event['progenitor'] == 'PBH-PBH':
                snrsq_vec = [self.snrsq_vec_PBH(z)]*len(self.survey.source_extinctions[ind,:])
            elif event['progenitor'] == 'testing':
                snrsq_vec = [self.snrsq_vec_testing(z)]*len(self.survey.source_extinctions[ind,:])
            else:
                print('Something is wrong!')
                snrsq_vec = None

            #snrsq_vec = self.snrfunc(z, z0, f0, waveform0, d_L, 3*[self.psd_etd])

            rho_tot = np.sqrt(sum([fact*snrsq_vec[arm] for arm,fact in enumerate(self.survey.source_extinctions[ind,:])]))

            if self.specs['include_lensing'] is True:
                mu          = self.lensing.draw_magnification(z)
                sigma_kappa = self.lensing.std_dev_convergence(z)
                # this is the relative error on the distance due to lensing
            else:
                mu          = 1
                sigma_kappa = 0

            # lensing affects the observed distance and the SNR
            d_lensed = d_unlensed * mu**(-0.5)
            rho_tot *= mu**0.5

            lens_error = d_lensed * sigma_kappa # absolute lensing error on distance
            inst_error = 2 * d_lensed / rho_tot # the factor 2 is a conservative choice

            event['dL_unlensed'] = d_unlensed
            event['dL'] = d_lensed

            mock['z'].append(z)
            mock['dL_unlensed'].append(d_unlensed)
            if self.specs['data_spread']:
                error      = np.sqrt(inst_error**2. + lens_error**2.)
                spread_min = (0. - d_lensed) / error
                spread_max = (1.e9 - d_lensed) / error
                mock['dL'].append(truncnorm.rvs(spread_min, spread_max, loc=d_lensed, scale=error))
            else:
                mock['dL'].append(d_lensed)
            mock['intrinsic_error'].append(inst_error)
            mock['lensing_error'].append(lens_error)
            mock['dL_error'].append(np.sqrt(inst_error**2. + lens_error**2.))
            mock['SNR'].append(rho_tot)
            mock['progenitor'].append(event['progenitor'])


        return mock
