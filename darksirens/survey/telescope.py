import numpy as np
from scipy.interpolate import interp1d
from scipy.integrate import quad

class survey:
    """
    This class handles the definitions of the survey being considered (only Einstein Telescope available for now).

    It is based on the gw-horizon code by Evan Hall.
    """

    def __init__(self,specs):

        self.specs = specs
        #This is not used now, but we might want to use it at some point
        #to switch between different survey

        #Getting random angles:

        #Source position
        theta = np.arccos(np.random.uniform(low=-1, high=1,size=specs['Ngw']))
        phi   = np.random.uniform(low=0, high=2*np.pi,size=specs['Ngw'])

        #Polarization
        psi   = np.random.uniform(low=0, high=2*np.pi,size=specs['Ngw'])

        #System inclination
        iota  = np.arccos(np.random.uniform(low=-1, high=1,size=specs['Ngw']))

        F_plus, F_cross = self.antenna(theta,phi,psi)

        aa = (1+np.cos(iota)**2) / 2
        bb = 1j * np.cos(iota)

        self.source_extinctions = (np.einsum('...a, ...', F_plus**2, np.abs(aa)**2)
                                  + np.einsum('...a, ...', F_cross**2, np.abs(bb)**2))

    def antenna(self,source_theta, source_phi, source_psi):
        """
        This function returns the antenna patterns for a telescope with 60 degree opening angles between its arms (equilateral triangle configuration.)

        :param source_theta: the right ascension angle of the source.
        :param source_phi: the declination angle of the source.
        :param source_psi: the polarisation angle (rotation angle between the source and detector planes).
        """


        #This to be changed if we want to switch surveys
        det_coords = np.array([[0, 0],[0, 0],[0, 0],])
        det_angles = np.array([[0, np.pi/3],[np.pi/3, np.pi/3],[2*np.pi/3, np.pi/3],])

        # Construct GW direction vectors
        source_n = np.array([
                             np.sin(source_theta)*np.cos(source_phi),
                             np.sin(source_theta)*np.sin(source_phi),
                             np.cos(source_theta),
                            ]).T
        source_ex = np.array([
                             np.sin(source_phi) * np.cos(source_psi) - np.cos(source_theta) * np.cos(source_phi) * np.sin(source_psi),
                             -np.cos(source_phi) * np.cos(source_psi) - np.cos(source_theta) * np.sin(source_phi) * np.sin(source_psi),
                             np.sin(source_theta) * np.sin(source_psi),
                            ]).T
        source_ey = np.array([
                             -np.sin(source_phi) * np.sin(source_psi) - np.cos(source_theta) * np.cos(source_phi) * np.cos(source_psi),
                             np.cos(source_phi) * np.sin(source_psi) - np.cos(source_theta) * np.sin(source_phi) * np.cos(source_psi),
                             np.sin(source_theta) * np.cos(source_psi),
                            ]).T

        # Construct detector direction vectors
        det_theta, det_phi = det_coords.T
        det_rho, det_alpha = det_angles.T

        det_n = np.array([
                          np.sin(det_theta)*np.cos(det_phi),
                          np.sin(det_theta)*np.sin(det_phi),
                          np.cos(det_theta),
                        ]).T
        det_ex = np.array([
                           np.sin(det_phi) * np.cos(-det_rho) - np.cos(det_theta) * np.cos(det_phi) * np.sin(-det_rho),
                           -np.cos(det_phi) * np.cos(-det_rho) - np.cos(det_theta) * np.sin(det_phi) * np.sin(-det_rho),
                           np.sin(det_theta) * np.sin(-det_rho),
                         ]).T
        det_ey = np.array([
                           np.sin(det_phi) * np.cos(-det_rho-det_alpha) - np.cos(det_theta) * np.cos(det_phi) * np.sin(-det_rho-det_alpha),
                           -np.cos(det_phi) * np.cos(-det_rho-det_alpha) - np.cos(det_theta) * np.sin(det_phi) * np.sin(-det_rho-det_alpha),
                           np.sin(det_theta) * np.sin(-det_rho-det_alpha),
                         ]).T

        # Construct source tensors
        source_plus = np.einsum('...a,...b', source_ex, source_ex) - np.einsum('...a,...b', source_ey, source_ey)
        source_cross = np.einsum('...a,...b', source_ex, source_ey) + np.einsum('...a,...b', source_ey, source_ex)

        # Construct detector freq resp
        Dx = 0.5*np.ones((source_n.shape[0], det_ex.shape[0]))
        Dy = 0.5*np.ones((source_n.shape[0], det_ex.shape[0]))

        Dx2 = np.einsum('ab,bc,bd->abcd', Dx, det_ex, det_ex)
        Dy2 = np.einsum('ab,bc,bd->abcd', Dy, det_ey, det_ey)

        #Construct detector tensor
        det_tensor = Dx2 - Dy2

        # Construct detector antenna responses
        det_plus = np.einsum('abc, adbc -> ad', source_plus, det_tensor)
        det_cross = np.einsum('abc, adbc -> ad', source_cross, det_tensor)

        det_plus = np.einsum('abc, adbc -> ad', source_plus, det_tensor)
        det_cross = np.einsum('abc, adbc -> ad', source_cross, det_tensor)

        return det_plus, det_cross
