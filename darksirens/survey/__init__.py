# -*- coding: utf-8 -*-

"""Events Module

In this module we include functions for the noise calculation given the detector(s) under consideration.

"""

__all__ = ['noisecalculation',
           'telescope']  # list submodules

from . import *
