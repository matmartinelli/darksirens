import numpy as np
from pycbc import waveform as wf
import scipy.constants as scc
from astropy import constants as const

class wave_properties:
    """
    This class handles the computation of the properties of the gravitational waves.
    """

    def __init__(self):

        pass

    def get_waveform(self,m1=30, m2=30, phic=0, distance=1e3):
        """
        This function returns the waveform (i.e. the GW strain :math:`h(f)`) from PyCBC.

        :param m1: the mass of the first component.
        :param m2: the mass of the second component.
        :param phic: the phase of the coalescence.
        :param distance: the luminosity distance to the event.
        """

        fisco = scc.c**3/(scc.G*6**1.5*2*np.pi*(m1+m2)*const.M_sun.value)
        df = 2**(np.max([np.floor(np.log(fisco/1024)/np.log(2)), -4]))
        mywf_p, mywf_c = wf.get_fd_waveform(
                                            approximant='IMRPhenomD',
                                            mass1=m1,
                                            mass2=m2,
                                            spin1z=0,
                                            spin2z=0,
                                            coa_phase=phic,
                                            distance=distance,
                                            delta_f = df,
                                            f_lower = 0.5,
                                            f_final = 0,
                                           )
        ff = np.array(mywf_p.sample_frequencies)
        mywfp = np.array(mywf_p.data)[ff>=1]
        ff = ff[ff>=1]
        return ff, mywfp
