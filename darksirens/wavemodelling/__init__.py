# -*- coding: utf-8 -*-

"""Wave Modelling Module

In this module we include functions for the modelling of the gravitational waves.

"""

__all__ = ['waves']  # list submodules

from . import *
