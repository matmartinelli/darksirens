import sys
import numpy as np
from copy import deepcopy

from scipy.interpolate import interp1d

class Cosmology:

    def __init__(self,params):

        self.params    = deepcopy(params)
        self.functions = self.get_cosmo_funcs(deepcopy(params))



    def get_cosmo_funcs(self,inipars):
        """
        Gets cosmological functions using camb.

        :param inipars: parameters passed to camb to get cosmological results.
        :type inipars: dictionary
        :return: Dictionary of cosmological functions.
        :rtype: dictionary
        """

        import camb

        params = camb.set_params(**self.camb_basis(inipars))

        results = camb.get_background(params)

        zlist = np.append(0, np.geomspace(1e-3, 1e2, 999))
        tlist = np.array([results.physical_time(z)*1e9 for z in zlist]) #Physical time in yr

        z_of_t = interp1d(tlist, zlist, kind='linear', bounds_error=False, fill_value=100.0)
        t_of_z = interp1d(zlist, tlist, kind='linear', bounds_error=True)

        cosmofuncs = {'Hubble': results.h_of_z, # H(z) in Mpc^-1
                      'Hubble_kmsMpc': results.hubble_parameter, # H(z) in km/s/Mpc
                      'angular_distance': results.angular_diameter_distance, #NHmod
                      'luminosity_distance': results.luminosity_distance,
                      'comoving_distance': results.comoving_radial_distance,
                      'z_of_t': z_of_t, #z(t [years]), starting with t = 0 at z = infinity
                      't_of_z': t_of_z, #t(z) [years], starting with t = 0 at z = infinity
                      'Omega_cdm': results.get_Omega('cdm', z = 0)}

        return cosmofuncs


    def camb_basis(self,cosmopars):
        """
        Checks for :math:`h` and :math:`\Omega_m` in the parameters passed and replaces them
        with :math:`H_0` and :math:`\Omega_ch^2`.

        :param cosmopars: cosmological parameters passed by the user.
        :type cosmopars: dictionary
        :return: The modified cosmopars dictionary.
        :rtype: dictionary
        """
        
        Omega_b_default = 0.05
        # Note: if you change this value, you may have to run again the
        # notebook "variance_convergence" and replace the fitting function
        # for the variance of convergence in lensing.py.

        if 'h' in cosmopars:
            cosmopars['H0'] = 100*cosmopars.pop('h')
        if 'Omega_m' in cosmopars:
            #MM: WARNING!!!!
            #This conversion assumes massless neutrinos.
            #It's ok if we only care about background.
            cosmopars['ombh2'] = Omega_b_default * (cosmopars['H0']/100)**2.
            cosmopars['omch2'] = (cosmopars.pop('Omega_m')
                                  - Omega_b_default)*(cosmopars['H0']/100)**2.


        return cosmopars
