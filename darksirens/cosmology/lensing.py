import numpy as np
from scipy.integrate import quad
from scipy.interpolate import interp1d


class Lensing:
    """
    This class manages the weak-lensing corrections to GW observations.
    It depends on the cosmology.
    """


    def __init__(self, cosmo):

        self.mu_min = self.get_minimal_magnification(cosmo=cosmo)
        
        H0 = cosmo.params['H0']
        Omega_m = cosmo.params['Omega_m']
        if (H0, Omega_m) != (67.4, 0.315):
            print("""Warning: In the current version of the code, the properties of lensing
have been computed for H0=67.4[km/s/Mpc], Omega_m=0.315 and Omega_b=0.05. For
different values you may have to run the notebook "variance_convergence.ipynb"
and substitute the new fitting function for sigma_kappa in "lensing.py".""")


    def get_minimal_magnification(self, cosmo, z_max=100, n_sample=1000):
        """
        Generates the minimal magnification up to :math:`z_{\\rm max}` by interpolating a
        sample of :math:`n_{\\rm sample}` elements.

        :param cosmo: instance of the Cosmology class.
        :param z_max: maximum redshift for the magnification.
        :type z_max: float
        :param n_sample: number of elements in the sample to be interpolated.
        :type n_sample: integer
        :return: The minimal magnification.
        :rtype: interp1d
        """

        H = cosmo.functions['Hubble']
        D_A = cosmo.functions['angular_distance']
        dldz = lambda z: 1 / H(z) / (1 + z)**2

        z_sample = np.append(0, np.logspace(-3, np.log(z_max), n_sample - 1))
        mu_min_sample = [1]

        for z in z_sample[1:]:
            l, err = quad(dldz, 0, z) # affine parameter at z
            D = D_A(z)
            mu_min = (D / l)**2
            #mu_min = float(mu_min)
            mu_min_sample.append(mu_min)
        mu_min_sample = np.array(mu_min_sample)

        mu_min_func = interp1d(z_sample, mu_min_sample)

        return mu_min_func


    def std_dev_convergence(self, z):
        """
        Standard deviation of the weak-lensing convergence.
        The expression is an empirical fit to the weak-lensing prediction,
        in the flat sky and Limber's approximation, and with a cosmology such that
        :math:`h=0.674`, :math:`\Omega_m=0.315` and :math:`\Omega_b=0.05`.

        **Warning:** To be changed depending on the cosmology. Run the notebook
        "variance_convergence" with the cosmology of your choice and replace
        the best-fit parameters a, b, c, d hereafter. 

        :param z: the redshift of the event.
        :type z: float
        """

        a, b, c, d = 0.11605285, 1.25725535, 1.46334517, 0.26809483
        sigma_kappa = a * np.arctan((1 + b * z**c)**d - 1)

        return sigma_kappa


    def pdf_magnification(self, mu, z):
        """
        Empirical PDF for the magnification at redshift :math:`z`. This is only for
        testing purposes.

        :param mu: the magnification.
        :type mu: float
        :param z: the redshift of the event.
        :type z: float
        """

        mu_min = self.mu_min(z)

        if mu > mu_min:
            sigma_mu = 2 * self.std_dev_convergence(z)
            sigma = (np.log(1 + (sigma_mu / (1 - mu_min))**2))**0.5
            m = - 0.5 * sigma**2 + np.log(1 - mu_min)
            p = np.exp( - 0.5 * (np.log(mu - mu_min) - m)**2 / sigma**2 )
            p /= (2 * np.pi)**0.5 * sigma * (mu - mu_min)
        else:
            p = 0

        return p


    def draw_magnification(self, z, size=None):
        """
        Randomly draws the magnification mu of an event, according to a shifted
        log-normal distribution. In other words, the quantity :math:`X = \ln(\mu - \mu_{\\rm min})`
        follows a normal distribution.
        The parameters of the distribution are chosen so as to ensure that
        :math:`\langle\mu\\rangle = 1`, in agreement with the magnification theorem; and
        :math:`\langle\mu^2\\rangle -  \langle\mu\\rangle^2 = 4 \sigma_\kappa^2`.

        :param z: the redshift of the event.
        :param size: output shape of the distribution drawn from (np.random.normal())
        """

        # parameters of the distribution
        mu_min = self.mu_min(z)
        sigma_mu = 2 * self.std_dev_convergence(z)
        sigma = (np.log(1 + (sigma_mu / (1 - mu_min))**2))**0.5
        m = - 0.5 * sigma**2 + np.log(1 - mu_min)

        # randomly draw the event
        X = np.random.normal(loc=m, scale=sigma, size=size)
        mu = mu_min + np.exp(X)

        return mu
