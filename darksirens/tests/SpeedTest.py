import sys
sys.path.append('../')
import numpy as np
import matplotlib.pyplot as plt

from cosmology.cosmo import Cosmology
from events.numberofevents import event_number_calculator

from timeit import default_timer as timer

clight = 299792.458

#Initialise some parameters
data_root = '../mock_data/ET_test'
specs   = np.load(data_root+'_specs.npy',allow_pickle=True).item()


PBHpars = {'PBHmass': 30.0,
               'fPBH': 0.001,
           'clustering': 'no'}

ABHpars = {'ABHmass': 7.,
               'z_m': 2.,
               'a':  2.37,
               'b': 1.8}

cosmopars = {'Omega_m': 0.32,
                 'H0': 67.}

cosmo = Cosmology(cosmopars)

#Initialise the event number and rate calculator
event_nums = event_number_calculator(cosmo,ABHpars,PBHpars,specs)

#THESE ARE THE OFFENDING FUNCTIONS!
#Actually it's just the PBH one...

z_list = np. geomspace(1e-3, 1e2, 100)
start = timer()
R_ABH = np.vectorize(event_nums.R_ABH_observed_z)(z_list, ABHpars)
end = timer()
ABH_time = end - start
start = timer()
R_PBH = np.vectorize(event_nums.R_PBH_observed_z)(z_list, PBHpars)
end = timer()
PBH_time = end - start


print("DONE...in " + str(PBH_time + ABH_time) + " seconds...")
print("                ... ABH calculation: " + str(ABH_time) + " seconds...")
print("                ... PBH calculation: " + str(PBH_time) + " seconds...")


#---------

plt.figure()

plt.semilogx(z_list, R_ABH, label="ABH")
plt.semilogx(z_list, R_PBH, label="PBH")

plt.title(r'$f_\mathrm{PBH} = 0.001$')
plt.xlabel(r"$z$")
plt.ylabel(r"$\mathcal{R}_\mathrm{obs}(z)$ [Gpc$^{-3}$ yr$^{-1}$]")

plt.axhline(0, linestyle='--', color='grey')

#plt.ylim(-10, 100)

plt.legend()
plt.savefig("/Users/bradkav/Desktop/NewTest.pdf")

plt.show()
