import sys
sys.path.append("..")

import numpy as np
from scipy.integrate import trapz, cumtrapz
from scipy.interpolate import interp1d
from scipy.stats import truncnorm
import random as r
from copy import deepcopy

from darksirens.wavemodelling.waves import wave_properties
from darksirens.events.pbhmergerrate import PBH_MRcalculator
from darksirens.events.abhmergerrate import ABH_MRcalculator

class event_generator:
    """
    This class handles the generation of events.
    """

    def __init__(self,cosmo,ABHpars,PBHpars,progenitor, specs):


        self.zcalc      = np.linspace(specs['z_min'],specs['z_max'],1000)
        self.cosmo      = cosmo
        self.PBH_MR     = PBH_MRcalculator(cosmo)
        self.PBHpars    = PBHpars
        self.ABH_MR     = ABH_MRcalculator(cosmo)
        self.ABHpars    = ABHpars
        waves           = wave_properties()

        self.z0         = 0.001
        d_L             = cosmo.functions['luminosity_distance']

        if progenitor == 'ABH-ABH' or progenitor == 'mixed':
            self.ABHrate = self.merger_rate(self.zcalc,'ABH-ABH',ABHpars['ABHmass'],ABHpars['ABHmass'])
            self.ABHprob = self.probz(self.cosmo,ABHpars['ABHmass'],ABHpars['ABHmass'],'ABH-ABH')
            self.ABH_frequency, self.ABH_waveform = waves.get_waveform(m1=ABHpars['ABHmass'], m2=ABHpars['ABHmass'], distance=d_L(self.z0))
        if progenitor == 'PBH-PBH' or progenitor == 'mixed':
            self.PBHrate = self.merger_rate(self.zcalc,'PBH-PBH',PBHpars['PBHmass'],PBHpars['PBHmass'])
            self.PBHprob = self.probz(self.cosmo,PBHpars['PBHmass'],PBHpars['PBHmass'],'PBH-PBH')
            self.PBH_frequency, self.PBH_waveform = waves.get_waveform(m1=PBHpars['PBHmass'], m2=PBHpars['PBHmass'], distance=d_L(self.z0))
        if progenitor == 'testing':
            self.test_frequency, self.test_waveform = waves.get_waveform(m1=ABHpars['ABHmass'], m2=ABHpars['ABHmass'], distance=d_L(self.z0))

    def get_single_event(self,progenitor):
        """
        This function updates the dictionary containing the event specifications.

        :param progenitor: progenitor type selected by the user.
        """
        #This function updates the dictionary containing the event specifications
        #It can probably be done in a much nicer way!

        final_pars = {'progenitor': progenitor,
                      'z_reference': self.z0}

        if progenitor == 'ABH-ABH':
            self.redshift_probability = self.ABHprob
            final_pars['frequency']   = self.ABH_frequency
            final_pars['waveform']    = self.ABH_waveform
        elif progenitor == 'PBH-PBH':
            self.redshift_probability = self.PBHprob
            final_pars['frequency']   = self.PBH_frequency
            final_pars['waveform']    = self.PBH_waveform
        elif progenitor == 'testing':
            final_pars['frequency']   = self.test_frequency
            final_pars['waveform']    = self.test_waveform




        self.get_redshift(final_pars)

        #MM: Maybe we could directly rescale the waveforms here

        return final_pars

    def get_redshift(self,final_pars):
        """
        This function assigns a redshift to the event. Redshifts are extracted from
        a probability distribution obtained from the merger rate.

        :param final_pars: the event parameters after restructuring.
        """
        #This function assigns a redshift to the event extracting it from
        #a probability distribution obtained from the merger rate

        if final_pars['progenitor'] == 'testing':
            final_pars['redshift'] = np.random.uniform(min(self.zcalc),max(self.zcalc))
        else:


            #final_pars['redshift'] = r.choices(self.zcalc, self.redshift_probability(self.zcalc)*self.zcalc)[0] # draw redshift from our custom normalised distribution
            #BJK - in the above, we need an extra factor self.zcalc, because the zcalc values are log-spaced

            #BJK: I've edited this to sample the redshift by inverse transform sampling.
            cumul_prob = cumtrapz(self.redshift_probability(self.zcalc), self.zcalc, initial = 0.0)
            final_pars['redshift'] = np.interp(np.random.uniform(), cumul_prob, self.zcalc)

        return None


    def probz(self,cosmo,m1,m2,progenitor):
        """
        This function creates the probability distribution from which the redshifts of the events are drawn.

        :param cosmo: an instance of the Cosmology class.
        :param m1: mass of the first component.
        :param m2: mass of the second component.
        :param progenitor: the progenitor type.
        """

        probability = {}


        if progenitor == 'ABH-ABH':
            rate  = self.ABHrate
        elif progenitor == 'PBH-PBH':
            rate  = self.PBHrate

        #BJK: Note that dVc/dz is also calculated with an independent piece of code in "dVcdz_full" of Nevents.py
        #BJK: Where does the 1/(1+z) come from? Ah! From the time part of the differential. (https://arxiv.org/abs/1704.04628)
        unnorm      = rate*(4*np.pi*(cosmo.functions['comoving_distance'](self.zcalc))**2)/(cosmo.functions['Hubble'](self.zcalc)*(1+self.zcalc))
        integral    = trapz(unnorm,x=self.zcalc)

        if integral != 0.:
            norm      = 1/integral
            norm_dist = norm*unnorm
        else:
            norm_dist = unnorm

        #BJK - kind = 'cubic' previously
        probability = interp1d(self.zcalc,norm_dist,kind='linear')

        return probability

    def merger_rate(self,zs,progenitor,m1,m2):
        """
        This function returns the merger rate given the progenitor type and the masses of the components.

        :param zs: array of event redshifts.
        :param progenitor: the type of progenitor: binary neutron star, binary ABH, binary PBH.
        :param m1: mass of the first component.
        :param m2: mass of the second component.
        """


        if progenitor == 'NS-NS':
            MR = np.array([(1+(2*z)) if z <=1 else (3/4)*(5-z) if 1 < z < 5 else 0 for z in zs])

        elif progenitor == 'ABH-ABH':
            MR = np.array([self.ABH_MR.R_z(z, self.ABHpars['z_m'], self.ABHpars['a'], self.ABHpars['b']) for z in zs])

        elif progenitor == 'PBH-PBH':

            mass=m1 #code works only with identical masses? #yes :(

            if self.PBHpars['clustering']=='yes':
                MR = np.array([self.PBH_MR.R_z_clust(z, self.PBHpars['fPBH'], mass) for z in zs])

            elif self.PBHpars['clustering']=='no':
                MR = np.array([self.PBH_MR.R_z(z, self.PBHpars['fPBH'], mass) for z in zs])


        return MR
