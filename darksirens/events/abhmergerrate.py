import numpy as np
from scipy.integrate import quad, trapz

class ABH_MRcalculator:
    """
    This class computes the merger rate of astrophysical black holes (ABHs).
    """

    def __init__(self, cosmo):

        self.year = 3600*24*365
        self.Myr = 1.e6 * self.year

        self.nu = 1. # NH: in the above paper this is 0.178?

        self.tmin = 50. * self.Myr
        self.tmax = self.t_univ(0, cosmo) * self.year

        #Interpolation functions for the age of the Universe at redshift z
        self.z_of_t = lambda t: cosmo.functions['z_of_t'](t/self.year)
        self.t_of_z = lambda z: cosmo.functions['t_of_z'](z)*self.year

    def t_univ(self, z, cosmo):
        """
        Returns the age of the Universe in years at a given redshift.

        :param z: the redshift
        :param cosmo: an instance of the Cosmology class.
        """
        age = cosmo.functions['t_of_z'](z)
        return age

    def star_formation_rate_z(self, z, z_m, a, b):
        """
        Returns the star formation rate as a function of redshift.
        See Springel & Hernquist 2002b and 2003, astro-ph/0206395 and astro-ph/0209183.

        :param z: the redshift.
        :param z_m: the break redshift.
        :param a: star formation rate parameter.
        :param b: star formation rate parameter.
        """
        return self.nu * a * np.exp(b*(z-z_m)) / ( a - b + b*np.exp(a*(z-z_m)) )

    def star_formation_rate(self, t, z_m, a, b):
        """
        Returns the star formation rate as a function of time.

        :param t: the time in yrs
        :param z_m: the break redshift.
        :param a: star formation rate parameter.
        :param b: star formation rate parameter.
        """
        return self.star_formation_rate_z(self.z_of_t(t), z_m, a, b)

    def delay_time_distribution(self, t_d):
        """
        Returns the distribution of the delay time between the start of star formation and the formation of
        the first ABHs.

        :param t_d: the delay time.
        """
        if np.isscalar(t_d):
            if (t_d >= self.tmin and t_d <= self.tmax):
                return 1.e10/t_d
            else:
                return 0.
        else:
            result = 0.0*t_d
            mask = (t_d >= self.tmin) & (t_d <= self.tmax)
            result[mask] = 1.e10/t_d[mask]
            return result


    def R_m(self, t_, z_m, a, b):
        """
        Returns the ABH merger rate as a function of time.

        :param t_: the time.
        :param z_m: the break redshift.
        :param a: star formation rate parameter.
        :param b: star formation rate parameter.
        """
        def integrand(x, z_m_, a_, b_):
            result = 0.0*x
            mask = (t_ - x) > 0
            result[mask] = self.star_formation_rate(t_ - x[mask], z_m_, a_, b_)*self.delay_time_distribution(x[mask])
            return result

        xlist = np.geomspace(self.tmin, self.tmax, 1000)
        #if (np.ndim(t_) == 0 or np.isscalar(t_)):
        result =  trapz(integrand(xlist, z_m,a,b),xlist)
        #X = np.broadcast_to(xlist, (len(xlist),len(t_)))
        #T = np.broadcast_to(t_, (len(xlist),len(t_)))
        return result

    def R_z(self, z_, z_m, a, b):
        """
        Returns the ABH merger rate as a function of redshift.

        :param z_: the redshift.
        :param z_m: the break redshift.
        :param a: star formation rate parameter.
        :param b: star formation rate parameter.
        """
        tvals = self.t_of_z(z_)
        return self.R_m(tvals, z_m, a, b)
