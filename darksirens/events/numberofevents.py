import os
dir_path = os.path.dirname(os.path.realpath(__file__))
import numpy as np

from scipy.integrate import quad, dblquad, trapz
from scipy.interpolate import interp1d, interp2d

from darksirens.events.pbhmergerrate import PBH_MRcalculator
from darksirens.events.abhmergerrate import ABH_MRcalculator

def events_integrator(integ, z1, z2, method='trapz'):
    """
    Generic function to integrate a merger rate between two redshifts
    with the option to set different integration methods (`trapz` or `quad`)
    In fact, we can probably generalise this as an integrator for anything...

    :param z1: start redshift.
    :param z2: end redshift.
    :param method: method of integration.
    """
    if (method == 'trapz'):
        z_list = np.linspace(z1, z2, 1000)
        try:
            integ_list = integ(z_list)
        except:
            integ_list = np.vectorize(integ)(z_list)
        result = np.trapz(integ_list, z_list)
    elif (method == 'quad'):
        result = quad(integ,z1, z2)[0]
    else:
        raise ValueError("Integration `method` <" + method + "> unknown...")
    return result

def detection_ratio_matrices(horizon_redshifts, redshift_grid, mTot_range, detection_coefficients):
    """
    This function creates a detection ratio matrix.

    :param horizon_redshifts: redshifts corresponding to the detection coefficients for the masses in mTot_range (obtained from Evan Hall's notebook here: https://git.ligo.org/evan.hall/gw-horizon-plot/-/blob/master/code/horizon_plot.ipynb)
    :param redshift_grid: grid of redshifts.
    :param mTot_range: range of total masses.
    :param detection_coefficients: coefficients which the horizon redshifts are interpolated between.
    """
    detection_ratio_matrix = np.zeros((len(redshift_grid), len(mTot_range)))
    for i in range(len(mTot_range)):
        #BJK: Double-check the extrapolation here!
        interpolator = interp1d(horizon_redshifts[i], detection_coefficients, kind = 'linear', bounds_error = False, fill_value = (1.0, 0.0))
        detection_ratio_matrix[:, i] = interpolator(redshift_grid)
    return detection_ratio_matrix

class event_number_calculator:
    """
    This class handles the calculation of the total number of events.
    """

    def __init__(self, cosmo, ABHpars, PBHpars, specs):

        self.year = 365.*24.*3600.

        self.Tligo = 1.23 * self.year  # O1: 49.4 days + O2: 124.4 days + O3a: 149.8 days + O3b: 125.5 days # see fig. 1 in https://arxiv.org/pdf/2111.03606.pdf

        self.TotalVT = 0.042  # Gpc**3 * yr  # see fig. 1 in https://arxiv.org/pdf/2111.03606.pdf

        z_list = np.append(0, np.linspace(1e-3, 1e2, 999))
        dVcdz_list = np.array([self.dVcdz_full(z, cosmo) for z in z_list])
        self.dVcdz_interp = interp1d(z_list, dVcdz_list)
        #self.zcalc = np.geomspace(specs['z_min'], specs['z_max'], 1000)
        self.zcalc = np.linspace(specs['z_min'], specs['z_max'], 1000)

        self.PBHtools = PBH_MRcalculator(cosmo)
        self.ABHtools = ABH_MRcalculator(cosmo)

        horizon_redshifts_LIGO = np.loadtxt(dir_path+'/aligo.dat') # contains the redshifts corresponding to the detection coefficients for the masses in mTot_range
        horizon_redshifts_ET = np.loadtxt(dir_path+'/ET.dat') # contains the redshifts corresponding to the detection coefficients for the masses in mTot_range

        redshift_grid_LIGO = np.logspace(-3, 1, 100)
        redshift_grid_ET = np.logspace(-3, 2, 100) #BJK: I've extended these grids down to 1e-3

        mTot_range = np.logspace(0, 3, 25)
        #mTot_range = np.logspace(1, 1.5, 5)
        detection_coefficients = 1 - np.linspace(0, 1, 100)

        detection_ratio_matrix_LIGO = detection_ratio_matrices(horizon_redshifts_LIGO, redshift_grid_LIGO, mTot_range, detection_coefficients)
        detection_ratio_matrix_ET = detection_ratio_matrices(horizon_redshifts_ET, redshift_grid_ET, mTot_range, detection_coefficients)

        self.detection_ratio_of_Mtot_z = interp2d(mTot_range, redshift_grid_LIGO, detection_ratio_matrix_LIGO, kind='linear', bounds_error = False, fill_value=0.)
        self.detection_ratio_of_Mtot_z_ET = interp2d(mTot_range, redshift_grid_ET, detection_ratio_matrix_ET, kind='linear', bounds_error = False, fill_value=0.)

        self.norm_ABH = self.compute_ABH_norm(ABHpars, PBHpars, cosmo)

    def dVcdz_full(self, z, cosmo):
        """
        This function returns the differential volume probed as a function of redshift.

        :param z: the redshift.
        :param cosmo: an instance of the Cosmology class.
        """
        clight = 299792.458 # km/s
        H0 = cosmo.params['H0'] # km/s/Mpc
        dH = (clight/H0)*(1e-9) # NH: we have a 1e-9 here to convert to Gpc since we don't have a dH appearing in dA
        E_of_z = cosmo.functions['Hubble_kmsMpc'](z)/H0 # dimensionless Hubble function E(z) = H(z)/H0
        dA = cosmo.functions['angular_distance'](z) # angular diameter distance
        full_volume = 4*np.pi*dH*(1+z)**2.*dA**2./E_of_z
        return full_volume


    def dVcdz(self, z, cosmo):
        """
        This function interpolates between the points computed for the probed comoving volume.

        :param z: the redshift.
        :param cosmo: an instance of the Cosmology class.
        """
        return self.dVcdz_interp(z)

    def dVTdz(self, z, cosmo, f, T0):
        """
        This function returns the differential sensitivity in units of Gpc^3 yr-1.

        :param z: the redshift.
        :param cosmo: an instance of the cosmology class.
        :param f: the ratio of :math:`\Omega_{\\rm PBH}` to :math:`\Omega_{\\rm CDM}`.
        :param T0: the length of the observation time in years.
        """
        return T0*self.dVcdz(z, cosmo)*(1/(1+z))*f

    def MRligoABH_calculator(self, ABHpars, cosmo):
        """
        This function computes the LIGO ABH merger rate.

        :param ABHpars: the ABH parameters passed by the user.
        :param cosmo: an instance of the cosmology class.
        """
        MABH = ABHpars['ABHmass']
        z_m  = ABHpars['z_m']
        a    = ABHpars['a']
        b    = ABHpars['b']

        num_integ= lambda x:self.ABHtools.R_z(x, z_m, a, b)*self.dVTdz(x, cosmo, self.detection_ratio_of_Mtot_z(2*MABH, x), self.Tligo)
        den_integ= lambda x:self.dVTdz(x, cosmo, self.detection_ratio_of_Mtot_z(2*MABH, x), self.Tligo)

        xint = np.linspace(0., 2., 1000)

        up   = trapz([num_integ(x)[0] for x in xint],x=xint)
        down = trapz([den_integ(x)[0] for x in xint],x=xint)

        return up/down


    def MRligoPBH_calculator(self, PBHpars, cosmo):
        """
        This function computes the LIGO PBH merger rate.

        :param PBHpars: the PBH parameters passed by the user.
        :param cosmo: an instance of the Cosmology class.
        """
        fPBH = PBHpars['fPBH']
        MPBH = PBHpars['PBHmass']

        if PBHpars['clustering']=='no':
            num_integ= lambda x: self.PBHtools.R_z(x, fPBH, MPBH)*self.dVTdz(x, cosmo, self.detection_ratio_of_Mtot_z(2*MPBH, x), self.Tligo)

        if PBHpars['clustering']=='yes':
            num_integ= lambda x: self.PBHtools.R_z_clust(x, fPBH, MPBH)*self.dVTdz(x, cosmo, self.detection_ratio_of_Mtot_z(2*MPBH, x), self.Tligo)

        den_integ= lambda x: self.dVTdz(x, cosmo, self.detection_ratio_of_Mtot_z(2*MPBH, x), self.Tligo)

        xint = np.linspace(0., 2., 1000)

        up   = trapz([num_integ(x)[0] for x in xint], x=xint)
        down = trapz([den_integ(x)[0] for x in xint], x=xint)

        return up/down

    def compute_ABH_norm(self, ABHpars, PBHpars, cosmo):
        """
        This function computes the normalisation of the ABH merger rate.

        :param ABHpars: the ABH parameters passed by the user.
        :param PBHpars: the PBH parameters passed by the user.
        :param cosmo: an instance of the Cosmology class.
        """
        Rligo = 22.  # Gpc^-3 yr^-1; from Table II in 2111.03634 (GWTC-3) # UPDATED BY DG, 21.02.2022

        N = (Rligo - self.MRligoPBH_calculator(PBHpars, cosmo))/self.MRligoABH_calculator(ABHpars, cosmo)

        return max(0, N)


    def normalized_ABH_MRcalculator(self, ABHpars, z):
        """
        This function computes the normalised ABH merger rate.

        :param ABHpars: the ABH parameters passed by the user.
        :param z: the redshift.
        """
        MABH = ABHpars['ABHmass']
        z_m  = ABHpars['z_m']
        a    = ABHpars['a']
        b    = ABHpars['b']

        return self.norm_ABH*self.ABHtools.R_z(z, z_m, a, b)

    def NPBHeventscalculator(self, PBHpars, cosmo, zmaxET, Tobs, method='trapz'):
        """
        This function computes the total possible number of PBH merger events the Einstein Telescope could see.

        :param PBHpars: the PBH parameters passed by the user.
        :param cosmo: an instance of the Cosmology class.
        :param zmaxET: the maximum redshift the Einstein Telescope will observe.
        :param Tobs: the length of the observing run in years.
        :param method: the interpolation method used.
        """

        MPBH = PBHpars['PBHmass']
        fPBH = PBHpars['fPBH']

        if PBHpars['clustering']=='no':
            integ = lambda x: self.PBHtools.R_z(x, fPBH, MPBH)*self.dVcdz(x, cosmo)*(1/(1+x))
        if PBHpars['clustering']=='yes':
            integ = lambda x: self.PBHtools.R_z_clust(x, fPBH, MPBH)*self.dVcdz(x, cosmo)*(1/(1+x))


        return Tobs*events_integrator(integ, np.min(self.zcalc), zmaxET, method)


    def NABHeventscalculator(self, ABHpars, cosmo, zmaxET, Tobs, method='trapz'):
        """
        This function computes the total possible number of ABH merger events the Einstein Telescope could see.

        :param ABHpars: the PBH parameters passed by the user.
        :param cosmo: an instance of the Cosmology class.
        :param zmaxET: the maximum redshift the Einstein Telescope will observe.
        :param Tobs: the length of the observing run in years.
        :param method: the interpolation method used.
        """
        integ= lambda x: self.normalized_ABH_MRcalculator(ABHpars, x)*self.dVcdz(x, cosmo)*(1/(1+x))
        return Tobs*events_integrator(integ, np.min(self.zcalc), zmaxET, method)


    def R_PBH_observed_z(self, z, PBHpars, cosmo):
        """
        Rate of observed PBH mergers, differential in z [Gpc^3 yr^-1].

        :param z: the redshift.
        :param PBHpars: the PBH parameters passed by the user.
        :param cosmo: an instance of the Cosmology class.
        """
        MPBH = PBHpars['PBHmass']
        fPBH = PBHpars['fPBH']

        if PBHpars['clustering'] == 'no':
            return self.PBHtools.R_z(z, fPBH, MPBH)*self.dVTdz(z, cosmo, self.detection_ratio_of_Mtot_z_ET(2*MPBH, z), T0=1.0)
        if PBHpars['clustering'] == 'yes':
            return self.PBHtools.R_z_clust(z, fPBH, MPBH)*self.dVTdz(z, cosmo, self.detection_ratio_of_Mtot_z_ET(2*MPBH, z), T0=1.0)

    def R_ABH_observed_z(self, z, ABHpars, cosmo):
        """
        Rate of observed ABH mergers, differential in z [Gpc^3 yr^-1].

        :param z: the redshift.
        :param ABHpars: the ABH parameters passed by the user.
        :param cosmo: an instance of the Cosmology class.
        """
        MABH = ABHpars['ABHmass']

        return self.normalized_ABH_MRcalculator(ABHpars, z)*self.dVTdz(z, cosmo, self.detection_ratio_of_Mtot_z_ET(2*MABH, z), T0=1.0)

    def NPBH_observed_eventscalculator(self, PBHpars, cosmo, zmaxET, Tobs, zmin, method='trapz'):
        """
        This function computes the number of PBH merger events the Einstein Telescope will actually observe.

        :param PBHpars: the PBH parameters passed by the user.
        :param cosmo: an instance of the Cosmology class.
        :param zmaxET: the maximum redshift the Einstein Telescope will observe.
        :param Tobs: the length of the observing run in years.
        :param method: the interpolation method used.
        """
        integ = lambda x: self.R_PBH_observed_z(x, PBHpars, cosmo)
        return Tobs*events_integrator(integ, zmin, zmaxET, method)

    def NABH_observed_eventscalculator(self, ABHpars, cosmo, zmaxET, Tobs, zmin, method='trapz'):
        """
        This function computes the number of PBH merger events the Einstein Telescope will actually observe.

        :param ABHpars: the ABH parameters passed by the user.
        :param cosmo: an instance of the Cosmology class.
        :param zmaxET: the maximum redshift the Einstein Telescope will observe.
        :param Tobs: the length of the observing run in years.
        :param method: the interpolation method used.
        """
        integ = lambda x: self.R_ABH_observed_z(x, ABHpars, cosmo)
        return Tobs*events_integrator(integ, zmin, zmaxET, method)
