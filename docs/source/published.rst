Publications
================================

Papers which make use of ``darksirens``:

* *Dancing in the dark: detecting a population of distant primordial black holes*. M. Martinelli et al., 2205.02639.

If you use ``darksirens`` in your work, let us know so we can add your paper here.
