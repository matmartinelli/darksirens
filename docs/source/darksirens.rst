The darksirens package
========================

Subpackages
-----------

.. toctree::

   darksirens.cosmology
   darksirens.events
   darksirens.survey
   darksirens.wavemodelling
