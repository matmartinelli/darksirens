Authors
========

The development of ``darksirens`` is lead by Matteo Martinelli,
with Francesca Scarcella, Natalie Hogg, Bradley Kavanagh, Daniele Gaggero and Pierre Fleury.


Contributors
-------------

Become a contributor and see your name here! To contribute, please fork ``darksirens``, open a new branch
on your fork and implement your changes, then open a pull request.
