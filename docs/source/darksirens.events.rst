Events
=========

darksirens.events.abhmergerrate module
-------------------------------------------------

.. automodule:: darksirens.events.abhmergerrate
   :members:
   :undoc-members:
   :show-inheritance:

darksirens.events.pbhmergerrate module
--------------------------------------------------

.. automodule:: darksirens.events.pbhmergerrate
   :members:
   :undoc-members:
   :show-inheritance:

darksirens.events.numberofevents module
--------------------------------------------------

.. automodule:: darksirens.events.numberofevents
   :members:
   :undoc-members:
   :show-inheritance:

darksirens.events.createevent module
----------------------------------------------------

.. automodule:: darksirens.events.createevent
   :members:
   :undoc-members:
   :show-inheritance:
