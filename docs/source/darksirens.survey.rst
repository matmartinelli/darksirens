Survey
=========

darksirens.survey.noisecalculation module
------------------------------------------

.. automodule:: darksirens.survey.noisecalculation
   :members:
   :undoc-members:
   :show-inheritance:

darksirens.survey.telescope module
------------------------------------------

.. automodule:: darksirens.survey.telescope
  :members:
  :undoc-members:
  :show-inheritance:
