.. Dark Sirens documentation master file, created by
   sphinx-quickstart on Wed Dec 15 15:57:55 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. include:: ../../README.rst

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   quickstart
   darksirens
   authors
   published
   history
   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
