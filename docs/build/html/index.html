
<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>darksirens: easy and realistic GW mock catalogues &#8212; darksirens</title>
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" />
    <link rel="stylesheet" href="_static/alabaster.css" type="text/css" />
    <script id="documentation_options" data-url_root="./" src="_static/documentation_options.js"></script>
    <script src="_static/jquery.js"></script>
    <script src="_static/underscore.js"></script>
    <script src="_static/doctools.js"></script>
    <script async="async" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.7/latest.js?config=TeX-AMS-MML_HTMLorMML"></script>
    <link rel="shortcut icon" href="_static/darksirens_favicon.ico"/>
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" />
    <link rel="next" title="Installation" href="installation.html" />
   
  <link rel="stylesheet" href="_static/custom.css" type="text/css" />
  
  
  <meta name="viewport" content="width=device-width, initial-scale=0.9, maximum-scale=0.9" />

  </head><body>
  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          

          <div class="body" role="main">
            
  <div class="section" id="darksirens-easy-and-realistic-gw-mock-catalogues">
<h1>darksirens: easy and realistic GW mock catalogues<a class="headerlink" href="#darksirens-easy-and-realistic-gw-mock-catalogues" title="Permalink to this headline">¶</a></h1>
<a class="reference external image-reference" href="http://darksirens.readthedocs.io/en/latest/?badge=latest"><img alt="Documentation Status" src="https://readthedocs.org/projects/darksirens/badge/?version=latest" /></a>
<a class="reference external image-reference" href="https://opensource.org/licenses/MIT"><img alt="https://img.shields.io/badge/licence-MIT-blue" src="https://img.shields.io/badge/licence-MIT-blue" /></a>
<a class="reference external image-reference" href="https://arxiv.org/abs/2205.02639"><img alt="https://img.shields.io/badge/arxiv-2205.02639-brightgreen" src="https://img.shields.io/badge/arxiv-2205.02639-brightgreen" /></a>
<p>The <code class="docutils literal notranslate"><span class="pre">darksirens</span></code> package quickly generates <strong>realistic</strong> mock catalogues of
luminosity distances, uncertainties and signal-to-noise ratios
that would be obtained from the measurements of gravitational waves from
binary black hole mergers made by the <strong>Einstein Telescope</strong>,
a proposed third-generation gravitational wave observatory.</p>
<p>One of the main features of <code class="docutils literal notranslate"><span class="pre">darksirens</span></code> is its ability to mock the observation of GWs from
<strong>primordial black hole</strong> mergers as well as astrophysical black hole mergers. This allows for the
assessment of the Einstein Telescope’s ability to <strong>detect</strong> these objects and <strong>measure</strong> their potential
contribution to the dark matter density, as reported in <a class="reference external" href="https://arxiv.org/abs/2205.02639">https://arxiv.org/abs/2205.02639</a>.</p>
<p>The package is <strong>easily extendible</strong> to include other observatories, different cosmologies (through its interface with <code class="docutils literal notranslate"><span class="pre">CAMB</span></code>),
different star formation and black hole merger rates, and many other possibilities.</p>
<div class="section" id="installation">
<h2>Installation<a class="headerlink" href="#installation" title="Permalink to this headline">¶</a></h2>
<p>Clone the git repository: <a class="reference external" href="https://gitlab.com/matmartinelli/darksirens">https://gitlab.com/matmartinelli/darksirens</a>.</p>
</div>
<div class="section" id="usage">
<h2>Usage<a class="headerlink" href="#usage" title="Permalink to this headline">¶</a></h2>
<p>See the <a class="reference internal" href="quickstart.html#quick-start-guide"><span class="std std-ref">Quick start guide</span></a>, or go straight to the <a class="reference external" href="https://gitlab.com/matmartinelli/darksirens/-/blob/master/darksirens/notebooks/Example.ipynb">example Jupyter notebook</a>
for more details.</p>
</div>
<div class="section" id="credit">
<h2>Credit<a class="headerlink" href="#credit" title="Permalink to this headline">¶</a></h2>
<p>If you use the <code class="docutils literal notranslate"><span class="pre">darksirens</span></code> package in your work, please cite this paper:</p>
<div class="highlight-latex notranslate"><div class="highlight"><pre><span></span>@article<span class="nb">{</span>darksirens,
 author = &quot;Martinelli M. and others&quot;,
 title = &quot;<span class="nb">{</span>Dancing in the dark: detecting a population
           of distant primordial black holes<span class="nb">}</span>&quot;,
 eprint = &quot;2205.02639&quot;,
 archivePrefix = &quot;arXiv&quot;,
 primaryClass = &quot;astro-ph.CO&quot;,
 year = &quot;2022&quot;
<span class="nb">}</span>
</pre></div>
</div>
</div>
<div class="section" id="feedback">
<h2>Feedback<a class="headerlink" href="#feedback" title="Permalink to this headline">¶</a></h2>
<p>For any comments, questions or contributions, contact the authors via <a class="reference external" href="mailto:matteo&#46;martinelli&#37;&#52;&#48;inaf&#46;it">matteo<span>&#46;</span>martinelli<span>&#64;</span>inaf<span>&#46;</span>it</a>.</p>
</div>
<div class="section" id="license">
<h2>License<a class="headerlink" href="#license" title="Permalink to this headline">¶</a></h2>
<p>The <code class="docutils literal notranslate"><span class="pre">darksirens</span></code> package is made freely available under the <a class="reference external" href="https://opensource.org/licenses/MIT">MIT license</a>.</p>
<div class="toctree-wrapper compound">
<p class="caption"><span class="caption-text">Contents:</span></p>
<ul>
<li class="toctree-l1"><a class="reference internal" href="installation.html">Installation</a></li>
<li class="toctree-l1"><a class="reference internal" href="quickstart.html">Quick start guide</a></li>
<li class="toctree-l1"><a class="reference internal" href="darksirens.html">The darksirens package</a><ul>
<li class="toctree-l2"><a class="reference internal" href="darksirens.html#subpackages">Subpackages</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="authors.html">Authors</a><ul>
<li class="toctree-l2"><a class="reference internal" href="authors.html#contributors">Contributors</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="published.html">Publications</a></li>
<li class="toctree-l1"><a class="reference internal" href="history.html">History</a></li>
<li class="toctree-l1"><a class="reference internal" href="modules.html">darksirens</a><ul>
<li class="toctree-l2"><a class="reference internal" href="darksirens.html">The darksirens package</a></li>
</ul>
</li>
</ul>
</div>
<div class="section" id="indices-and-tables">
<h3>Indices and tables<a class="headerlink" href="#indices-and-tables" title="Permalink to this headline">¶</a></h3>
<ul class="simple">
<li><p><a class="reference internal" href="genindex.html"><span class="std std-ref">Index</span></a></p></li>
<li><p><a class="reference internal" href="py-modindex.html"><span class="std std-ref">Module Index</span></a></p></li>
<li><p><a class="reference internal" href="search.html"><span class="std std-ref">Search Page</span></a></p></li>
</ul>
</div>
</div>
</div>


          </div>
          
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
            <p class="logo"><a href="#">
              <img class="logo" src="_static/darksirens_logo.png" alt="Logo"/>
            </a></p>
<h1 class="logo"><a href="#">darksirens</a></h1>








<h3>Navigation</h3>
<p class="caption"><span class="caption-text">Contents:</span></p>
<ul>
<li class="toctree-l1"><a class="reference internal" href="installation.html">Installation</a></li>
<li class="toctree-l1"><a class="reference internal" href="quickstart.html">Quick start guide</a></li>
<li class="toctree-l1"><a class="reference internal" href="darksirens.html">The darksirens package</a></li>
<li class="toctree-l1"><a class="reference internal" href="authors.html">Authors</a></li>
<li class="toctree-l1"><a class="reference internal" href="published.html">Publications</a></li>
<li class="toctree-l1"><a class="reference internal" href="history.html">History</a></li>
<li class="toctree-l1"><a class="reference internal" href="modules.html">darksirens</a></li>
</ul>

<div class="relations">
<h3>Related Topics</h3>
<ul>
  <li><a href="#">Documentation overview</a><ul>
      <li>Next: <a href="installation.html" title="next chapter">Installation</a></li>
  </ul></li>
</ul>
</div>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" />
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script>$('#searchbox').show(0);</script>








        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="footer">
      &copy;2022, M. Martinelli, F. Scarcella, N. B. Hogg, B. J. Kavanagh, D. Gaggero, P. Fleury.
      
      |
      Powered by <a href="http://sphinx-doc.org/">Sphinx 3.4.2</a>
      &amp; <a href="https://github.com/bitprophet/alabaster">Alabaster 0.7.12</a>
      
      |
      <a href="_sources/index.rst.txt"
          rel="nofollow">Page source</a>
    </div>

    

    
  </body>
</html>