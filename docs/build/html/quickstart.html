
<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Quick start guide &#8212; darksirens</title>
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" />
    <link rel="stylesheet" href="_static/alabaster.css" type="text/css" />
    <script id="documentation_options" data-url_root="./" src="_static/documentation_options.js"></script>
    <script src="_static/jquery.js"></script>
    <script src="_static/underscore.js"></script>
    <script src="_static/doctools.js"></script>
    <script async="async" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.7/latest.js?config=TeX-AMS-MML_HTMLorMML"></script>
    <link rel="shortcut icon" href="_static/darksirens_favicon.ico"/>
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" />
    <link rel="next" title="The darksirens package" href="darksirens.html" />
    <link rel="prev" title="Installation" href="installation.html" />
   
  <link rel="stylesheet" href="_static/custom.css" type="text/css" />
  
  
  <meta name="viewport" content="width=device-width, initial-scale=0.9, maximum-scale=0.9" />

  </head><body>
  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          

          <div class="body" role="main">
            
  <div class="section" id="quick-start-guide">
<h1>Quick start guide<a class="headerlink" href="#quick-start-guide" title="Permalink to this headline">¶</a></h1>
<p>This page demonstrates how to make your first GW mock catalogue, containing both astrophysical
and primordial black holes. Further uses of the code are shown in the
<a class="reference external" href="https://gitlab.com/matmartinelli/darksirens/-/blob/master/darksirens/notebooks/Example.ipynb">example notebook</a>.</p>
<p>First, we import the relevant packages:</p>
<div class="highlight-python notranslate"><div class="highlight"><pre><span></span><span class="kn">import</span> <span class="nn">numpy</span> <span class="k">as</span> <span class="nn">np</span>
<span class="kn">import</span> <span class="nn">matplotlib.pyplot</span> <span class="k">as</span> <span class="nn">plt</span>
<span class="kn">from</span> <span class="nn">darksirens.mock_maker</span> <span class="kn">import</span> <span class="n">compute_GWmock</span>
</pre></div>
</div>
<p>Specify the cosmological parameters (the code assumes flat <span class="math notranslate nohighlight">\(\Lambda\)</span> CDM for now),</p>
<div class="highlight-python notranslate"><div class="highlight"><pre><span></span><span class="n">cosmopars</span> <span class="o">=</span> <span class="p">{</span><span class="s1">&#39;Omega_m&#39;</span><span class="p">:</span> <span class="mf">0.315</span><span class="p">,</span>
             <span class="s1">&#39;H0&#39;</span><span class="p">:</span> <span class="mf">67.4</span><span class="p">}</span>
</pre></div>
</div>
<p>Specify:</p>
<ul class="simple">
<li><p>the survey (only Einstein Telescope in the ET-D configuration available at the moment),</p></li>
<li><p>the minimum and maximum redshifts</p></li>
<li><p>the length of the observing run in years</p></li>
<li><p>the minimum signal-to-noise ratio for a viable GW detection</p></li>
<li><p>whether to include the effect of weak gravitational lensing on the signal and the noise</p></li>
<li><p>whether to add a random spread to the conversion from redshift (unknown in reality) to luminosity distance (only measurable quantity in reality).</p></li>
</ul>
<div class="highlight-python notranslate"><div class="highlight"><pre><span></span><span class="n">specs</span> <span class="o">=</span> <span class="p">{</span><span class="s1">&#39;survey&#39;</span><span class="p">:</span> <span class="s1">&#39;ET-D&#39;</span><span class="p">,</span>
         <span class="s1">&#39;z_min&#39;</span><span class="p">:</span> <span class="mf">0.001</span><span class="p">,</span>
         <span class="s1">&#39;z_max&#39;</span><span class="p">:</span> <span class="mi">100</span><span class="p">,</span>
         <span class="s1">&#39;Tobs&#39;</span><span class="p">:</span> <span class="mi">1</span><span class="p">,</span>
         <span class="s1">&#39;SNR_min&#39;</span><span class="p">:</span> <span class="mi">8</span><span class="p">,</span>
         <span class="s1">&#39;include_lensing&#39;</span><span class="p">:</span> <span class="kc">False</span><span class="p">,</span>
         <span class="s1">&#39;data_spread&#39;</span><span class="p">:</span> <span class="kc">False</span><span class="p">}</span>
</pre></div>
</div>
<p>Set the progenitor type and the ABH and PBH parameters. For the ABHs we have the mass and three parameters which control the merger rate.
For the PBHs, we have the mass, whether or not to take clustering into account, and the fraction of dark matter contained
in PBHs.</p>
<div class="highlight-python notranslate"><div class="highlight"><pre><span></span><span class="n">progenitor</span> <span class="o">=</span> <span class="s1">&#39;mixed&#39;</span>

<span class="n">ABHpars</span> <span class="o">=</span> <span class="p">{</span><span class="s1">&#39;ABHmass&#39;</span><span class="p">:</span> <span class="mi">7</span><span class="p">,</span>
           <span class="s1">&#39;z_m&#39;</span><span class="p">:</span> <span class="mf">2.</span><span class="p">,</span>
           <span class="s1">&#39;a&#39;</span><span class="p">:</span> <span class="mf">2.37</span><span class="p">,</span>
           <span class="s1">&#39;b&#39;</span><span class="p">:</span> <span class="mf">1.8</span><span class="p">}</span>

<span class="n">PBHpars</span> <span class="o">=</span> <span class="p">{</span><span class="s1">&#39;PBHmass&#39;</span><span class="p">:</span> <span class="mi">10</span><span class="p">,</span>
           <span class="s1">&#39;clustering&#39;</span><span class="p">:</span> <span class="s1">&#39;yes&#39;</span><span class="p">,</span>
           <span class="s1">&#39;fPBH&#39;</span><span class="p">:</span> <span class="mf">1.e-5</span><span class="p">}</span>
</pre></div>
</div>
<p>Compute the mock catalogue. You can add an optional path to this function if you want to save the data to file.</p>
<div class="highlight-python notranslate"><div class="highlight"><pre><span></span><span class="n">data</span> <span class="o">=</span> <span class="n">compute_GWmock</span><span class="p">(</span><span class="n">cosmopars</span><span class="p">,</span> <span class="n">progenitor</span><span class="p">,</span> <span class="n">ABHpars</span><span class="p">,</span> <span class="n">PBHpars</span><span class="p">,</span> <span class="n">specs</span><span class="p">)</span>
</pre></div>
</div>
<p>You can now plot your mock data, for example the distances and their errors,</p>
<div class="highlight-python notranslate"><div class="highlight"><pre><span></span><span class="n">plt</span><span class="o">.</span><span class="n">figure</span><span class="p">(</span><span class="n">figsize</span> <span class="o">=</span> <span class="p">(</span><span class="mi">10</span><span class="p">,</span><span class="mi">8</span><span class="p">))</span>
<span class="n">plt</span><span class="o">.</span><span class="n">errorbar</span><span class="p">(</span><span class="n">data</span><span class="o">.</span><span class="n">mock</span><span class="p">[</span><span class="s1">&#39;z&#39;</span><span class="p">],</span> <span class="n">data</span><span class="o">.</span><span class="n">mock</span><span class="p">[</span><span class="s1">&#39;dL&#39;</span><span class="p">],</span>
             <span class="n">yerr</span> <span class="o">=</span> <span class="n">data</span><span class="o">.</span><span class="n">mock</span><span class="p">[</span><span class="s1">&#39;dL_error&#39;</span><span class="p">],</span>
             <span class="n">ls</span> <span class="o">=</span> <span class="s1">&#39;&#39;</span><span class="p">,</span> <span class="n">marker</span> <span class="o">=</span> <span class="s1">&#39;*&#39;</span><span class="p">,</span> <span class="n">color</span> <span class="o">=</span> <span class="s1">&#39;black&#39;</span><span class="p">)</span>
<span class="n">plt</span><span class="o">.</span><span class="n">xlabel</span><span class="p">(</span><span class="sa">r</span><span class="s1">&#39;$z$&#39;</span><span class="p">)</span>
<span class="n">plt</span><span class="o">.</span><span class="n">ylabel</span><span class="p">(</span><span class="sa">r</span><span class="s1">&#39;$d_L(z)$ Mpc&#39;</span><span class="p">)</span>
<span class="n">plt</span><span class="o">.</span><span class="n">show</span><span class="p">()</span>
</pre></div>
</div>
<img alt="_images/mock.png" src="_images/mock.png" />
<p>More detailed information and further uses for the code are shown in the
<a class="reference external" href="https://gitlab.com/matmartinelli/dark_sirens/-/blob/master/darksirens/notebooks/Example.ipynb">example notebook</a>.</p>
<p><em>Every program has (at least) two purposes: the one for which it was written, and another for which it wasn’t.</em> — Alan Perlis</p>
</div>


          </div>
          
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
            <p class="logo"><a href="index.html">
              <img class="logo" src="_static/darksirens_logo.png" alt="Logo"/>
            </a></p>
<h1 class="logo"><a href="index.html">darksirens</a></h1>








<h3>Navigation</h3>
<p class="caption"><span class="caption-text">Contents:</span></p>
<ul class="current">
<li class="toctree-l1"><a class="reference internal" href="installation.html">Installation</a></li>
<li class="toctree-l1 current"><a class="current reference internal" href="#">Quick start guide</a></li>
<li class="toctree-l1"><a class="reference internal" href="darksirens.html">The darksirens package</a></li>
<li class="toctree-l1"><a class="reference internal" href="authors.html">Authors</a></li>
<li class="toctree-l1"><a class="reference internal" href="published.html">Publications</a></li>
<li class="toctree-l1"><a class="reference internal" href="history.html">History</a></li>
<li class="toctree-l1"><a class="reference internal" href="modules.html">darksirens</a></li>
</ul>

<div class="relations">
<h3>Related Topics</h3>
<ul>
  <li><a href="index.html">Documentation overview</a><ul>
      <li>Previous: <a href="installation.html" title="previous chapter">Installation</a></li>
      <li>Next: <a href="darksirens.html" title="next chapter">The darksirens package</a></li>
  </ul></li>
</ul>
</div>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" />
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script>$('#searchbox').show(0);</script>








        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="footer">
      &copy;2022, M. Martinelli, F. Scarcella, N. B. Hogg, B. J. Kavanagh, D. Gaggero, P. Fleury.
      
      |
      Powered by <a href="http://sphinx-doc.org/">Sphinx 3.4.2</a>
      &amp; <a href="https://github.com/bitprophet/alabaster">Alabaster 0.7.12</a>
      
      |
      <a href="_sources/quickstart.rst.txt"
          rel="nofollow">Page source</a>
    </div>

    

    
  </body>
</html>