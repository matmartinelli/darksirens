���/      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]��docutils.nodes��section���)��}�(hhh]�(h	�title���)��}�(h�Quick start guide�h]�h	�Text����Quick start guide�����}�(hh�parent�hhh�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�N/home/natalie/Documents/Projects/public_dark_sirens/docs/source/quickstart.rst�hKubh	�	paragraph���)��}�(hX  This page demonstrates how to make your first GW mock catalogue, containing both astrophysical
and primordial black holes. Further uses of the code are shown in the
`example notebook <https://gitlab.com/matmartinelli/darksirens/-/blob/master/darksirens/notebooks/Example.ipynb>`_.�h]�(h��This page demonstrates how to make your first GW mock catalogue, containing both astrophysical
and primordial black holes. Further uses of the code are shown in the
�����}�(h��This page demonstrates how to make your first GW mock catalogue, containing both astrophysical
and primordial black holes. Further uses of the code are shown in the
�hh.hhhNhNubh	�	reference���)��}�(h�r`example notebook <https://gitlab.com/matmartinelli/darksirens/-/blob/master/darksirens/notebooks/Example.ipynb>`_�h]�h�example notebook�����}�(h�example notebook�hh9ubah}�(h ]�h"]�h$]�h&]�h(]��name��example notebook��refuri��\https://gitlab.com/matmartinelli/darksirens/-/blob/master/darksirens/notebooks/Example.ipynb�uh*h7hh.ubh	�target���)��}�(h�_ <https://gitlab.com/matmartinelli/darksirens/-/blob/master/darksirens/notebooks/Example.ipynb>�h]�h}�(h ]��example-notebook�ah"]�h$]�h&]��example notebook�ah(]��refuri�hKuh*hL�
referenced�Khh.ubh�.�����}�(h�.�hh.hhhNhNubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hKhhhhubh-)��}�(h�'First, we import the relevant packages:�h]�h�'First, we import the relevant packages:�����}�(hhihhghhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hKhhhhubh	�literal_block���)��}�(h�cimport numpy as np
import matplotlib.pyplot as plt
from darksirens.mock_maker import compute_GWmock�h]�h�cimport numpy as np
import matplotlib.pyplot as plt
from darksirens.mock_maker import compute_GWmock�����}�(hhhhwubah}�(h ]�h"]�h$]�h&]�h(]��	xml:space��preserve��force���language��python��highlight_args�}�uh*huhh+hK
hhhhubh-)��}�(h�XSpecify the cosmological parameters (the code assumes flat :math:`\Lambda` CDM for now),�h]�(h�;Specify the cosmological parameters (the code assumes flat �����}�(h�;Specify the cosmological parameters (the code assumes flat �hh�hhhNhNubh	�math���)��}�(h�:math:`\Lambda`�h]�h�\Lambda�����}�(hhhh�ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h�hh�ubh� CDM for now),�����}�(h� CDM for now),�hh�hhhNhNubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hKhhhhubhv)��}�(h�7cosmopars = {'Omega_m': 0.315,
             'H0': 67.4}�h]�h�7cosmopars = {'Omega_m': 0.315,
             'H0': 67.4}�����}�(hhhh�ubah}�(h ]�h"]�h$]�h&]�h(]�h�h�h��h��python�h�}�uh*huhh+hKhhhhubh-)��}�(h�Specify:�h]�h�Specify:�����}�(hh�hh�hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hKhhhhubh	�bullet_list���)��}�(hhh]�(h	�	list_item���)��}�(h�Wthe survey (only Einstein Telescope in the ET-D configuration available at the moment),�h]�h-)��}�(hh�h]�h�Wthe survey (only Einstein Telescope in the ET-D configuration available at the moment),�����}�(hh�hh�ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hKhh�ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h�hh�hhhh+hNubh�)��}�(h�!the minimum and maximum redshifts�h]�h-)��}�(hh�h]�h�!the minimum and maximum redshifts�����}�(hh�hh�ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hKhh�ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h�hh�hhhh+hNubh�)��}�(h�(the length of the observing run in years�h]�h-)��}�(hj  h]�h�(the length of the observing run in years�����}�(hj  hj  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hKhj  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h�hh�hhhh+hNubh�)��}�(h�;the minimum signal-to-noise ratio for a viable GW detection�h]�h-)��}�(hj  h]�h�;the minimum signal-to-noise ratio for a viable GW detection�����}�(hj  hj  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hKhj  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h�hh�hhhh+hNubh�)��}�(h�Wwhether to include the effect of weak gravitational lensing on the signal and the noise�h]�h-)��}�(hj3  h]�h�Wwhether to include the effect of weak gravitational lensing on the signal and the noise�����}�(hj3  hj5  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hKhj1  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h�hh�hhhh+hNubh�)��}�(h��whether to add a random spread to the conversion from redshift (unknown in reality) to luminosity distance (only measurable quantity in reality).
�h]�h-)��}�(h��whether to add a random spread to the conversion from redshift (unknown in reality) to luminosity distance (only measurable quantity in reality).�h]�h��whether to add a random spread to the conversion from redshift (unknown in reality) to luminosity distance (only measurable quantity in reality).�����}�(hjN  hjL  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hKhjH  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h�hh�hhhh+hNubeh}�(h ]�h"]�h$]�h&]�h(]��bullet��*�uh*h�hh+hKhhhhubhv)��}�(h��specs = {'survey': 'ET-D',
         'z_min': 0.001,
         'z_max': 100,
         'Tobs': 1,
         'SNR_min': 8,
         'include_lensing': False,
         'data_spread': False}�h]�h��specs = {'survey': 'ET-D',
         'z_min': 0.001,
         'z_max': 100,
         'Tobs': 1,
         'SNR_min': 8,
         'include_lensing': False,
         'data_spread': False}�����}�(hhhjh  ubah}�(h ]�h"]�h$]�h&]�h(]�h�h�h��h��python�h�}�uh*huhh+hK hhhhubh-)��}�(hX  Set the progenitor type and the ABH and PBH parameters. For the ABHs we have the mass and three parameters which control the merger rate.
For the PBHs, we have the mass, whether or not to take clustering into account, and the fraction of dark matter contained
in PBHs.�h]�hX  Set the progenitor type and the ABH and PBH parameters. For the ABHs we have the mass and three parameters which control the merger rate.
For the PBHs, we have the mass, whether or not to take clustering into account, and the fraction of dark matter contained
in PBHs.�����}�(hjz  hjx  hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hK*hhhhubhv)��}�(h��progenitor = 'mixed'

ABHpars = {'ABHmass': 7,
           'z_m': 2.,
           'a': 2.37,
           'b': 1.8}

PBHpars = {'PBHmass': 10,
           'clustering': 'yes',
           'fPBH': 1.e-5}�h]�h��progenitor = 'mixed'

ABHpars = {'ABHmass': 7,
           'z_m': 2.,
           'a': 2.37,
           'b': 1.8}

PBHpars = {'PBHmass': 10,
           'clustering': 'yes',
           'fPBH': 1.e-5}�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�h�h�h��h��python�h�}�uh*huhh+hK.hhhhubh-)��}�(h�oCompute the mock catalogue. You can add an optional path to this function if you want to save the data to file.�h]�h�oCompute the mock catalogue. You can add an optional path to this function if you want to save the data to file.�����}�(hj�  hj�  hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hK;hhhhubhv)��}�(h�Edata = compute_GWmock(cosmopars, progenitor, ABHpars, PBHpars, specs)�h]�h�Edata = compute_GWmock(cosmopars, progenitor, ABHpars, PBHpars, specs)�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�h�h�h��h��python�h�}�uh*huhh+hK=hhhhubh-)��}�(h�LYou can now plot your mock data, for example the distances and their errors,�h]�h�LYou can now plot your mock data, for example the distances and their errors,�����}�(hj�  hj�  hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hKAhhhhubhv)��}�(h��plt.figure(figsize = (10,8))
plt.errorbar(data.mock['z'], data.mock['dL'],
             yerr = data.mock['dL_error'],
             ls = '', marker = '*', color = 'black')
plt.xlabel(r'$z$')
plt.ylabel(r'$d_L(z)$ Mpc')
plt.show()�h]�h��plt.figure(figsize = (10,8))
plt.errorbar(data.mock['z'], data.mock['dL'],
             yerr = data.mock['dL_error'],
             ls = '', marker = '*', color = 'black')
plt.xlabel(r'$z$')
plt.ylabel(r'$d_L(z)$ Mpc')
plt.show()�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�h�h�h��h��python�h�}�uh*huhh+hKChhhhubh	�image���)��}�(h�.. image:: mock.png

�h]�h}�(h ]�h"]�h$]�h&]�h(]��uri��mock.png��
candidates�}�jg  j�  suh*j�  hhhhhh+hKOubh-)��}�(h��More detailed information and further uses for the code are shown in the
`example notebook <https://gitlab.com/matmartinelli/dark_sirens/-/blob/master/darksirens/notebooks/Example.ipynb>`_.�h]�(h�IMore detailed information and further uses for the code are shown in the
�����}�(h�IMore detailed information and further uses for the code are shown in the
�hj�  hhhNhNubh8)��}�(h�s`example notebook <https://gitlab.com/matmartinelli/dark_sirens/-/blob/master/darksirens/notebooks/Example.ipynb>`_�h]�h�example notebook�����}�(h�example notebook�hj�  ubah}�(h ]�h"]�h$]�h&]�h(]��name��example notebook�hJ�]https://gitlab.com/matmartinelli/dark_sirens/-/blob/master/darksirens/notebooks/Example.ipynb�uh*h7hj�  ubhM)��}�(h�` <https://gitlab.com/matmartinelli/dark_sirens/-/blob/master/darksirens/notebooks/Example.ipynb>�h]�h}�(h ]��id1�ah"]�h$]�h&]�hXah(]��refuri�j�  uh*hLh[Khj�  ubh�.�����}�(hh`hj�  hhhNhNubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hKPhhhhubh-)��}�(h�*Every program has (at least) two purposes: the one for which it was written, and another for which it wasn't.* --- Alan Perlis�h]�(h	�emphasis���)��}�(h�o*Every program has (at least) two purposes: the one for which it was written, and another for which it wasn't.*�h]�h�oEvery program has (at least) two purposes: the one for which it was written, and another for which it wasn’t.�����}�(hhhj  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*j  hj  ubh� — Alan Perlis�����}�(h� --- Alan Perlis�hj  hhhNhNubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hKShhhhubeh}�(h ]��quick-start-guide�ah"]�h$]��quick start guide�ah&]�h(]�uh*h
hhhhhh+hKubah}�(h ]�h"]�h$]�h&]�h(]��source�h+uh*h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j]  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h+�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�(j7  j4  �example notebook�Nu�	nametypes�}�(j7  Nj�  �uh }�(j4  hhThNj  j�  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}�jk  Ks��R��parse_messages�]�h	�system_message���)��}�(hhh]�h-)��}�(h�3Duplicate explicit target name: "example notebook".�h]�h�7Duplicate explicit target name: “example notebook”.�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h,hj�  ubah}�(h ]�h"]�h$]�h&]�h(]�j  a�level�K�type��WARNING��source�h+�line�Kuh*j�  hhhhhh+hKQuba�transform_messages�]��transformer�N�
decoration�Nhhub.