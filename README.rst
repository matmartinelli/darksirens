==============================================================
darksirens: easy and realistic GW mock catalogues
==============================================================

.. image:: https://readthedocs.org/projects/darksirens/badge/?version=latest
        :target: http://darksirens.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status

.. image:: https://img.shields.io/badge/licence-MIT-blue
        :target: https://opensource.org/licenses/MIT

.. image:: https://img.shields.io/badge/arxiv-2205.02639-brightgreen
        :target: https://arxiv.org/abs/2205.02639

The ``darksirens`` package quickly generates **realistic** mock catalogues of
luminosity distances, uncertainties and signal-to-noise ratios
that would be obtained from the measurements of gravitational waves from
binary black hole mergers made by the **Einstein Telescope**,
a proposed third-generation gravitational wave observatory.

One of the main features of ``darksirens`` is its ability to mock the observation of GWs from
**primordial black hole** mergers as well as astrophysical black hole mergers. This allows for the
assessment of the Einstein Telescope's ability to **detect** these objects and **measure** their potential
contribution to the dark matter density, as reported in https://arxiv.org/abs/2205.02639.

The package is **easily extendible** to include other observatories, different cosmologies (through its interface with ``CAMB``),
different star formation and black hole merger rates, and many other possibilities.

Installation
-------------
Clone the git repository: https://gitlab.com/matmartinelli/darksirens.

Usage
-----------------
See the :ref:`Quick start guide`, or go straight to the `example Jupyter notebook <https://gitlab.com/matmartinelli/darksirens/-/blob/master/darksirens/notebooks/Example.ipynb>`_
for more details.

Credit
-----------
If you use the ``darksirens`` package in your work, please cite this paper:

.. code-block:: latex

   @article{darksirens,
    author = "Martinelli M. and others",
    title = "{Dancing in the dark: detecting a population
              of distant primordial black holes}",
    eprint = "2205.02639",
    archivePrefix = "arXiv",
    primaryClass = "astro-ph.CO",
    year = "2022"
   }

Feedback
-----------
For any comments, questions or contributions, contact the authors via matteo.martinelli@inaf.it.


License
---------
The ``darksirens`` package is made freely available under the `MIT license <https://opensource.org/licenses/MIT>`_.
